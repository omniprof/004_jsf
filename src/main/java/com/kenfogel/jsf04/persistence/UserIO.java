package com.kenfogel.jsf04.persistence;

import com.kenfogel.jsf04.model.UserBean;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is similar to the persistence class in the examples 001, 002, 003. It is
 * now a managed bean that will be instantiated by CDI upon first use. The
 * method addRecord no longer takes any parameters. The userBean is injected by
 * the framework and the filename is retrieved from the web.xml here.
 *
 * To keep the examples simple persistence is achieved by writing to a text file
 * rather than to a database. It could be replaced with a Data Access Object
 * (DAO) without the need to modify anything else due to adherence to the
 * principle of Separation of Concerns (S0C)
 *
 * This is the backing action bean for the form. Here an action is called on the
 * xhtml page by referencing the name, userIOBean, with the method name,
 * addRecord, to become #{userIOBean.addRecord}. It is in Session scope so that
 * every visitor shares the same bean as we want to write to the same file. The
 * method addRecord is synchronized so that the writes complete for each visitor
 * before the next visitor add to the file.
 *
 * @author Ken Fogel
 */
@Named("userIOBean") // DO NOT USE @Managed, this annotation is obsolete now
@SessionScoped
public class UserIO implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(UserIO.class);

    // The magic of CDI. The bean that was bound to the form that issues the 
    // action will be the bean that is injected.
    @Inject
    private UserBean userBean;

    /**
     * This method writes the data in the userBean to a file. In previous years
     * I taught that it was possible to write this file into the folder that
     * contained the web application. This is no longer the case as its a bad
     * practice. You must never write into the directory structure of a web app.
     * Instead you should write to an absolute location that can be configured
     * in the web.xml file.
     *
     * The method is synchronized to make it thread safe
     *
     * Thank you to Tom Schindl @tomsontom for pointing out that rather than
     * declaring the character set as Charset.forName("UTF-8") it should be
     * StandardCharsets.UTF_8
     *
     * @return a string that is used to determine the navigation rule found in
     * faces-config.xml. There is only one rule so the method only returns
     * 'success'
     * @throws IOException potential error when writing to the file
     */
    public synchronized String addRecord() throws IOException {
        //Retrieve the context-param with the file path
        String filename = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("EmailFile");
        LOG.debug("Filename = " + filename);
        Path emailList = Paths.get(filename);
        try (BufferedWriter writer = Files.newBufferedWriter(emailList, StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(userBean.getEmailAddress() + "|"
                    + userBean.getFirstName() + "|"
                    + userBean.getLastName() + "\n");
        }
        return "success";
    }
}
